#!/bin/env python3

#    SpaceFlightCal downloads and converts datetimes for rocket launches
#    from an obscure website.
#    Copyright (C) 2016 Ruben De Smet
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib.request
import parsedatetime as pdt
from icalendar import Calendar, Event, vText
from bs4 import BeautifulSoup
from datetime import date

url = "http://spaceflightnow.com/launch-schedule/"

def fetch_and_parse():
    req = urllib.request.urlopen(url)
    soup = BeautifulSoup(req.read(), 'html.parser')
    data = parse(soup)
    create_ics(data)

def create_ics(data):
    cal = Calendar()
    cal.add('prodid', '-//SpaceFlightCal//rubdos.be//')
    cal.add('version', '2.0')
    for datum in data:
        if datum['datetime'] is None:
            print("Could not parse the date of event " + event['summary'])
            continue
        event = Event()
        event.add('dtstart', datum['datetime'])
        event['summary'] =     vText(datum['name'])
        event['location'] =    vText(datum['site'])
        event['description'] = vText(datum['description'])
        cal.add_component(event)
    f = open("events.ics", 'wb')
    f.write(cal.to_ical())
    f.close()

def parse_date(launch_date, launch_time):
    c = pdt.Calendar()
    (date, status) = c.parseDT(launch_date)
    # 0 = not parsed at all
    # 1 = parsed as a C{date}
    # 2 = parsed as a C{time}
    # 3 = parsed as a C{datetime}
    if status == 1:
        return date
    else:
        return None

def parse(soup):
    dates = soup.find_all('div', "datename")

    data = []
    for date in dates:
        # Twice next_sibling... Possibly a bug in bs4?
        missiondata = date.next_sibling.next_sibling
        missiondesc = missiondata.next_sibling.next_sibling

        (launch_date, mission_name) = date.find_all('span')
        mission_name = mission_name.contents[0]
        launch_date = launch_date.contents[0]
        launch_time = missiondata.contents[1]

        launch_site = missiondata.contents[3]
        mission_desc = missiondesc.contents[0]

        data.append({
            "name": mission_name,
            "site": launch_site,
            "datetime": parse_date(launch_date, launch_time),
            "description": mission_desc
            })
    return data

if __name__ == "__main__":
    fetch_and_parse()
